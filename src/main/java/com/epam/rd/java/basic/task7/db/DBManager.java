package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private String FULL_URL;


	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try(InputStream inputStream = new FileInputStream("app.properties")) {
			Properties properties = new Properties();
			properties.load(inputStream);
			FULL_URL = properties.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> userList = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			Statement stm = connection.createStatement();
			ResultSet resultSet = stm.executeQuery("SELECT * FROM users")){
			while (resultSet.next()){
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
				userList.add(user);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			PreparedStatement stm = connection.prepareStatement("INSERT INTO users (login) VALUES (?)", Statement.RETURN_GENERATED_KEYS))
		{
			stm.setString(1,user.getLogin());

			stm.execute();
			ResultSet rs = stm.getGeneratedKeys();
			if (rs.next()) {
				user.setId(rs.getInt(1));
			}
			return true;
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			PreparedStatement stm = connection.prepareStatement("DELETE FROM users WHERE login=?"))
		{
			for(User user: users){
				stm.setString(1,user.getLogin());
				stm.executeUpdate();
			}

			return 	true;
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return false;
	}

	public User getUser(String login) throws DBException {
	    User user = new User();
        try(Connection connection = DriverManager.getConnection(FULL_URL);
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM users WHERE login=?"))
        {
            stm.setString(1,login);
            ResultSet resultSet = stm.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
		return user;
	}

	public Team getTeam(String name) throws DBException {
        Team team = new Team();
        try(Connection connection = DriverManager.getConnection(FULL_URL);
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM teams WHERE name=?"))
        {
            stm.setString(1,name);
            ResultSet resultSet = stm.executeQuery();
            if (resultSet.next()) {
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamList = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			Statement stm = connection.createStatement();
			ResultSet resultSet = stm.executeQuery("SELECT * FROM teams")){
			while (resultSet.next()){
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				teamList.add(team);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		try(Connection connection = DriverManager.getConnection(FULL_URL);
			PreparedStatement stm = connection.prepareStatement("INSERT INTO teams (name) VALUES (?)",Statement.RETURN_GENERATED_KEYS))
		{
			stm.setString(1,team.getName());

			stm.execute();
			ResultSet rs = stm.getGeneratedKeys();
			int id = 0;
			if (rs.next()){
				id = rs.getInt(1);
			}
			team.setId(id);
			return true;
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DriverManager.getConnection(FULL_URL);
			connection.setAutoCommit(false);
			try {
				List<Team> teamList = findAllTeams();
				for (Team t: teams){
					int teamId = 0;
					if (!teamList.contains(t)){
						statement = connection.prepareStatement("INSERT INTO teams (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
						statement.setString(1,t.getName());
						statement.executeQuery();
						ResultSet rs = statement.getGeneratedKeys();
						if (rs.next()){
							teamId = rs.getInt(1);
						}
					}
					else{
						teamId = teamList.get(teamList.indexOf(t)).getId();
					}

					PreparedStatement stm = connection.prepareStatement("INSERT INTO users_teams " +
							"(user_id, team_id) VALUES (?,?)");
					stm.setString(1, String.valueOf(user.getId()));
					stm.setString(2, String.valueOf(teamId));
					stm.executeUpdate();

				}

			} catch (SQLException e) {
				connection.rollback();
				//e.printStackTrace();
				throw new DBException("", e);
			}
		   connection.commit();
			return true;
		}
		catch (SQLException e){
			if (connection != null) {
				try {
					connection.rollback();
					throw new DBException("", e);
				} catch (SQLException throwables) {
					throw new DBException("", e);
					//throwables.printStackTrace();
				}
			}
			e.printStackTrace();
		}
		finally {
			if (statement != null){
				try{
					statement.close();
				} catch (SQLException throwables) {
					throwables.printStackTrace();

				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException throwables) {
					throwables.printStackTrace();
				}
			}
		}
		return false;	
	}

	public List<Team> getUserTeams(User user) throws DBException {

		List<Team> teamList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		try{
			connection = DriverManager.getConnection(FULL_URL);
			connection.setAutoCommit(false);
			try{
				statement = connection.prepareStatement("SELECT * FROM teams " +
						"JOIN users_teams ON teams.id=users_teams.team_id " +
						"JOIN users ON users.id=users_teams.user_id " +
						"WHERE users.id=?");
				statement.setString(1,String.valueOf(user.getId()));
				ResultSet resultSet = statement.executeQuery();
				while (resultSet.next()){
					Team team = new Team();
					team.setId(resultSet.getInt("id"));
					team.setName(resultSet.getString("name"));
					teamList.add(team);
				}
			}
			catch (SQLException e){
				e.printStackTrace();
				connection.rollback();
			}
			connection.commit();
		}
		catch (SQLException e){
			if (connection != null) {
				try {
					connection.rollback();
				} catch (SQLException throwables) {
					throwables.printStackTrace();
				}
			}
			e.printStackTrace();
		}
		finally {
			if (statement != null){
				try{
					statement.close();
				}
				catch (SQLException e){
					e.printStackTrace();
				}
			}
			if (connection != null){
				try{
					connection.close();
				}
				catch (SQLException e){
					e.printStackTrace();
				}
			}
		}
		return teamList;
	}

	public boolean deleteTeam(Team team) throws DBException {
        try(Connection connection = DriverManager.getConnection(FULL_URL);
            PreparedStatement stm = connection.prepareStatement("DELETE FROM teams WHERE name=?"))
        {
            stm.setString(1,team.getName());
            stm.executeUpdate();
            return 	true;
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return false;
	}

	public boolean updateTeam(Team team) throws DBException {
	    try (Connection connection = DriverManager.getConnection(FULL_URL);
            PreparedStatement statement = connection.prepareStatement("UPDATE teams SET name=? WHERE id=?"))
        {
            statement.setString(1, team.getName());
			statement.setString(2,String.valueOf(team.getId()));
            statement.executeUpdate();
            return true;
        }
	    catch (SQLException e){
	        e.printStackTrace();
        }
		return false;
	}

}
